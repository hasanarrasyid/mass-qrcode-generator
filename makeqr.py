#!/usr/bin/python3

import csv, qrcode, sys, os

os.mkdir("QRCODE")

namafile  = sys.argv

with open(namafile[1], newline='') as csvfile:
    csvreader = csv.reader(csvfile, delimiter=';', quotechar='|')
    img = 0
    for row in csvreader:
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=0,
        )
        print(f'I.{row[1]}.S', img)
        qr.add_data(f'I.{row[1]}.S')
        imgcreate = qr.make_image(fill_color="black", back_color="white")
        imgfile = '{:0>2}'.format(img)
        imgcreate.save(f'QRCODE/{imgfile}.jpg')
        img = img+1

os.remove("QRCODE/00.jpg")
